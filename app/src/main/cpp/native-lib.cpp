#include <jni.h>
#include <string>

#define ARRAY_LENGTH    5

extern "C" JNIEXPORT jobject JNICALL
Java_id_ac_ui_cs_mobileprogramming_naufal_1ihsan_1pratama_clone_1app_addtweet_AddTweetFragment_generateTweet(
        JNIEnv *env,
        jobject /* this */) {

    jobjectArray sentence;
    int i, n;

    char *sentences[5] = {"Aku belajar semalaman", "Saya makan sebentar", "Dia bekerja seharian",
                          "Anda bermain sekejap", "Kalian tertidur sewindu"};

    sentence = (jobjectArray) env->NewObjectArray(5, env->FindClass("java/lang/String"),
                                                  env->NewStringUTF(""));

    for (i = 0; i < 5; i++) {
        env->SetObjectArrayElement(
                sentence, i, env->NewStringUTF(sentences[i]));
    }

    n = rand() % 5;

    jobject result = env->GetObjectArrayElement(sentence, n);

    return result;

}

