package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.find

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import kotlinx.android.synthetic.main.fragment_find.*
import kotlinx.android.synthetic.main.fragment_home.*

class FindFragment : Fragment() {

    private lateinit var viewModel: FindViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(FindViewModel::class.java)
        return inflater.inflate(R.layout.fragment_find, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(FindViewModel::class.java)
        viewModel.refesh()

        val listAdapter = FindAdapter(arrayListOf(), viewModel)

        itemsList2.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }

        observeViewModel(listAdapter)

    }

    fun observeViewModel(listAdapter: FindAdapter) {
        viewModel.users.observe(this, Observer { user ->
            user?.let {
                if (it.isNotEmpty()) {
                    itemsList2.visibility = View.VISIBLE
                    listAdapter.updateList(user)
                } else {
                    emptyState2.visibility = View.VISIBLE
                }
            }
        })

        viewModel.error.observe(this, Observer { isError ->
            isError?.let {
                emptyState2.visibility = if (it) View.VISIBLE else View.GONE
            }

        })

        viewModel.loading.observe(this, Observer { isLoading ->
            isLoading?.let {
                loadingView3.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    emptyState2.visibility = View.GONE
                    itemsList2.visibility = View.GONE
                }
            }
        })

    }
}