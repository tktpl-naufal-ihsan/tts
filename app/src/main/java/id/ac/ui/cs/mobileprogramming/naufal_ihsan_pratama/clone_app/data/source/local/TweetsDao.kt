package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Tweet

@Dao
interface TweetsDao {
    @Query("SELECT * FROM Tweet JOIN User ON Tweet.username = User.username WHERE Tweet.username IN (SELECT following FROM Follow WHERE follower = :username) OR Tweet.username = :username")
    suspend fun getAllTweet(username: String): List<Tweet>

    @Insert
    suspend fun addTweet(tweet: Tweet)
}