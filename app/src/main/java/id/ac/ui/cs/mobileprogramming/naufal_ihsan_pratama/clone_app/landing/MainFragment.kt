package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.landing

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.databinding.MainFragmentBinding
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R


class MainFragment : Fragment() {

    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.signInTxt.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToSigninFragment()
            findNavController().navigate(action)
        }

        binding.signUpBtn.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToSignupFragment()
            findNavController().navigate(action)
        }

    }

}
