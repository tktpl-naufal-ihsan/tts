package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source

import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Follow
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.FollowsDao

class FollowsRepository(private val followsDao: FollowsDao) {
    suspend fun getFollower(username: String): Int {
        return followsDao.getFollower(username)
    }

    suspend fun getFollowing(username: String): Int {
        return followsDao.getFollowing(username)
    }

    suspend fun findUsers(username: String): List<User> {
        return followsDao.findUsers(username)
    }

    suspend fun follow(follow: Follow) {
        return followsDao.isFollow(follow)
    }

}