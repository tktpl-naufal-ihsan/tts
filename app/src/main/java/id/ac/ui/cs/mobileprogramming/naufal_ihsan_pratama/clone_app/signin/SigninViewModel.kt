package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.signin

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.UsersRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.CloneDatabase
import kotlinx.coroutines.launch

class SigninViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: UsersRepository

    val user = MutableLiveData<User>()

    init {
        val userDao = CloneDatabase.getDatabase(application).userDao()
        repository = UsersRepository(userDao)
    }

    fun signIn(username: String, password: String) = viewModelScope.launch {
        val login = repository.auth(username, password)
        user.value = login
        if (user.value != null) {
            Toast.makeText(
                getApplication(),
                "Log in as " + user.value!!.username,
                Toast.LENGTH_SHORT
            )
                .show()
        } else {
            Toast.makeText(getApplication(), "Login Failed", Toast.LENGTH_SHORT)
                .show()
        }
    }
}
