package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.home

import android.app.Application
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Tweet
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.TweetsRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.CloneDatabase
import kotlinx.coroutines.launch

class ListViewModel(application: Application) : AndroidViewModel(application) {
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "clone-app"

    val users = MutableLiveData<List<Tweet>>()
    val error = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    private val repository: TweetsRepository

    init {
        val tweetsDao = CloneDatabase.getDatabase(application).tweetDao()
        repository = TweetsRepository(tweetsDao)
    }

    fun refesh() {
        val localPref: SharedPreferences =
            getApplication<Application>().getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val username = localPref.getString("username", "user")!!
        getTweet(username)
        error.value = false
        loading.value = false
    }

    fun getTweet(username: String) = viewModelScope.launch {
        users.value = repository.get(username).sortedWith(compareByDescending { it.timestamp })
    }
}
