package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Tweet
import kotlinx.android.synthetic.main.item_list.view.*
import kotlin.collections.List

class ListAdapter(val list: ArrayList<Tweet>) :

    RecyclerView.Adapter<ListAdapter.UserViewHolder>() {

    fun updateList(newList: List<Tweet>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_list, parent, false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.view.username.text = "@".plus(list[position].username)
        holder.view.tweet.text = list[position].tweet
        if (list[position].imgurl != "-") {
            Glide.with(holder.view).load(list[position].imgurl).into(holder.view.tweetImg);
        } else {
            Glide.with(holder.view).load(R.drawable.placeholder).into(holder.view.tweetImg);
        }
    }

    class UserViewHolder(var view: View) : RecyclerView.ViewHolder(view)
}
