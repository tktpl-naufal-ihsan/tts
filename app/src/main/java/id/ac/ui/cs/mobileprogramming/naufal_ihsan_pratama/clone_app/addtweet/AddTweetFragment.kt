package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.addtweet

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.home.Homepage
import kotlinx.android.synthetic.main.add_tweet_fragment.*

import java.io.File
import java.lang.Exception

class AddTweetFragment : Fragment() {

    companion object {
        fun newInstance() = AddTweetFragment()

        init {
            System.loadLibrary("native-lib")
        }

    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */

    private external fun generateTweet(): String


    private lateinit var viewModel: AddTweetViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.add_tweet_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddTweetViewModel::class.java)

        viewModel.fetch()

        viewModel.user.observe(this, Observer {
            try {
                if (it.imgurl != "-") {
                    Glide.with(this).load(it.imgurl).into(addTweetImg)
                } else {
                    Glide.with(this)
                        .load(R.drawable.placeholder)
                        .into(addTweetImg)
                }

            } catch (e: Exception) {
                Glide.with(this).load(Uri.fromFile(File(it.imgurl))).into(addTweetImg)
            }
        })

        addTweetBtn.setOnClickListener {
            val text = tweetContent.text.toString()

            if (text.isNotEmpty()) {
                viewModel.addTweet(text)

                viewModel.event.observe(this, Observer {
                    if (it) {
                        val intent = Intent(context, Homepage::class.java)
                        startActivity(intent)
                    }
                })
            } else {

                Toast.makeText(context, "Generate Random Tweet", Toast.LENGTH_SHORT)
                    .show()

                tweetContent.setText(generateTweet())
            }
        }
    }

}
