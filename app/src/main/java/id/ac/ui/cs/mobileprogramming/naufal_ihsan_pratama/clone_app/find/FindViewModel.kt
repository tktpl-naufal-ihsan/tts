package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.find

import android.app.Application
import androidx.lifecycle.*
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Follow
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.FollowsRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.CloneDatabase
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.util.NotificationsHelper
import kotlinx.coroutines.launch

class FindViewModel(application: Application) : AndroidViewModel(application) {
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "clone-app"


    private val sharedPrefs by lazy {
        getApplication<Application>().getSharedPreferences(
            PREF_NAME,
            PRIVATE_MODE
        )
    }

    var username: String = ""

    val users = MutableLiveData<List<User>>()
    val error = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    private val repository: FollowsRepository

    init {
        val followsDao = CloneDatabase.getDatabase(application).followDao()
        repository = FollowsRepository(followsDao)
        username = sharedPrefs.getString("username", "user")!!
    }

    fun refesh() {
        findUsers(username)
        error.value = false
        loading.value = false
    }

    fun findUsers(uname: String) = viewModelScope.launch {
        users.value = repository.findUsers(uname)
    }

    fun following(target: String) = viewModelScope.launch {
        repository.follow(Follow(0, target, username))
        NotificationsHelper(getApplication()).createNofitication(
            "Following Success",
            username.plus(" is following ").plus(target)
        )
    }

}