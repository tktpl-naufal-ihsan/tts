package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.landing

import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.NavController
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.BaseActivity
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R

class MainActivity : BaseActivity() {

    private lateinit var navController: NavController
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = Navigation.findNavController(this,
            R.id.nav_host_fragment
        )
        NavigationUI.setupActionBarWithNavController(this, navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, null)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish()
            moveTaskToBack(false)
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }


}
