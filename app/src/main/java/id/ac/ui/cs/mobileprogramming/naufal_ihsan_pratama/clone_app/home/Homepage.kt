package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.home

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.BaseActivity
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.addtweet.AddTweet
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.landing.MainActivity
import java.util.*

class Homepage : BaseActivity() {

    private var PRIVATE_MODE = 0
    private val PREF_NAME = "clone-app"
    private val READ_EXTERNAL_REQUEST_CODE = 101

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homepage)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        setupPermissions()

        val localPref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        with(localPref.edit()) {
            if (intent.hasExtra("username")) {
                putString("username", intent.getStringExtra("username"))
                putString("imgurl", intent.getStringExtra("imgurl"))
                commit()
            }
        }

        val fab: FloatingActionButton = this.findViewById(R.id.fab)

        fab.setOnClickListener {
            val intent = Intent(applicationContext, AddTweet::class.java)
            startActivityForResult(intent, 0)
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView = findViewById<NavigationView>(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        val username = navView.getHeaderView(0).findViewById<TextView>(R.id.username)
        val profpict = navView.getHeaderView(0).findViewById<ImageView>(R.id.tweetImg)


        val uname = localPref.getString("username", "-")
        val uimage = localPref.getString("imgurl", "-")

        if (uname != "-") {
            username.text = "@".plus(uname)
        }

        if (uimage != "-") {
            Glide.with(this).load(uimage).into(profpict)
        } else {
            Glide.with(this).load(R.drawable.placeholder).into(profpict)
        }

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_find,
                R.id.nav_profile
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }


    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            READ_EXTERNAL_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            READ_EXTERNAL_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("PERM", "Permission has been denied by user")
                } else {
                    Log.i("PERM", "Permission has been granted by user")
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        menuInflater.inflate(R.menu.homepage, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.english) {
            updateLocale(Locale.ENGLISH)
            return true
        } else if (item.itemId == R.id.france) {
            updateLocale(Locale.FRANCE)
            return true
        } else if (item.itemId == R.id.logout) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item);
    }

    override fun updateLocale(locale: Locale) {
        super.updateLocale(locale)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
