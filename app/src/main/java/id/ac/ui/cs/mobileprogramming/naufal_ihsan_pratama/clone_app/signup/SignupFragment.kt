package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.signup

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.databinding.SignupFragmentBinding
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.home.Homepage
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import kotlinx.android.synthetic.main.main_fragment.signUpBtn
import kotlinx.android.synthetic.main.signup_fragment.*


class SignupFragment : Fragment() {

    private lateinit var viewModel: SignupViewModel
    private lateinit var binding: SignupFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.signup_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this)
            .get(SignupViewModel::class.java)
        signUpBtn.setOnClickListener {
            val username = signUpUsername.text.toString()
            val email = signUpEmail.text.toString()
            val password = signUpPass.text.toString()

            viewModel.signUp(username, email, password)

            viewModel.user.observe(this, Observer {
                if (it != null) {
                    val intent = Intent(
                        context,
                        Homepage::class.java
                    )
                    intent.putExtra("username", it.username)
                    intent.putExtra("imgurl", it.imgurl)
                    startActivity(intent)
                }
            })
        }
    }
}
