package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source

import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Tweet
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.TweetsDao

class TweetsRepository(private val tweetsDao: TweetsDao) {
    suspend fun get(username: String): List<Tweet> {
        return tweetsDao.getAllTweet(username)
    }

    suspend fun add(tweet:Tweet) {
        return tweetsDao.addTweet(tweet)
    }

}