package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source

import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.ConsolidateWeather
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class WeatherService {

    private val BASE_URL = "https://www.metaweather.com"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(WeatherApi::class.java)

    fun getWeather(): Single<ConsolidateWeather> {
        return api.getWeather()
    }
}