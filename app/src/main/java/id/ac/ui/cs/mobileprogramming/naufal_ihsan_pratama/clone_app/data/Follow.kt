package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "follow")
data class Follow @JvmOverloads constructor(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "following") var following: String = "",
    @ColumnInfo(name = "follower") var follower: String = ""
){
    val isEmpty
        get() = follower.isEmpty() || following.isEmpty()
}
