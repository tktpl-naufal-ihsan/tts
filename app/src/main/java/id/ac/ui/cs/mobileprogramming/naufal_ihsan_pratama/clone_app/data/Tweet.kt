package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Timestamp

@Entity(tableName = "tweet")
data class Tweet @JvmOverloads constructor(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "username") var username: String = "",
    @ColumnInfo(name = "tweet") var tweet: String = "",
    @ColumnInfo(name = "imgurl") var imgurl: String = "",
    @ColumnInfo(name = "timestamp") var timestamp: Long? = null
) {
    val isEmpty
        get() = username.isEmpty()
}
