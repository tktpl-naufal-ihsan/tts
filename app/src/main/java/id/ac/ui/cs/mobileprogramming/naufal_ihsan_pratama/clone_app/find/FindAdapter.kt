package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.find

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import kotlinx.android.synthetic.main.item_find_list.view.*
import kotlinx.android.synthetic.main.item_list.view.tweetImg
import kotlinx.android.synthetic.main.item_list.view.username
import java.io.File
import java.lang.Exception

class FindAdapter(val list: ArrayList<User>, val viewModel: FindViewModel) :

    RecyclerView.Adapter<FindAdapter.UserViewHolder>() {

    fun updateList(newList: List<User>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_find_list, parent, false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.view.username.text = "@".plus(list[position].username)
        try {
            if (list[position].imgurl != "-") {
                Glide.with(holder.view).load(list[position].imgurl).into(holder.view.tweetImg);
            } else {
                Glide.with(holder.view).load(R.drawable.placeholder).into(holder.view.tweetImg);
            }
        } catch (e: Exception) {
            Glide.with(holder.view).load(Uri.fromFile(File(list[position].imgurl)))
                .into(holder.view.tweetImg)
        }

        holder.view.followBtn.setOnClickListener {
            viewModel.following(list[position].username)
            viewModel.refesh()
        }
    }

    class UserViewHolder(var view: View) : RecyclerView.ViewHolder(view)
}
