package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app

import android.content.res.Configuration
import android.util.Log
import android.view.ContextThemeWrapper
import androidx.appcompat.app.AppCompatActivity
import com.zeugmasolutions.localehelper.LocaleAwareCompatActivity
import java.util.*

open class BaseActivity : LocaleAwareCompatActivity() {}