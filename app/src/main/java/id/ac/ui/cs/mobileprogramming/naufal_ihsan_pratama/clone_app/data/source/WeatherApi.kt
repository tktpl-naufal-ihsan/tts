package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source

import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.ConsolidateWeather
import io.reactivex.Single
import retrofit2.http.GET

interface WeatherApi {
    @GET("api/location/1047378")
    fun getWeather(): Single<ConsolidateWeather>
}