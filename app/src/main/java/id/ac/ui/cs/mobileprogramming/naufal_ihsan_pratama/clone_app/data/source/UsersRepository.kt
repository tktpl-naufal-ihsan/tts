package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source

import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.UsersDao

class UsersRepository(private val usersDao: UsersDao) {

    suspend fun auth(username: String, password: String): User? {
        return usersDao.authenticate(username, password)
    }

    suspend fun get(username: String): User? {
        return usersDao.getUser(username)
    }

    suspend fun insert(user: User) {
        usersDao.addUser(user)
    }

    suspend fun update(username: String, uri: String) {
        usersDao.updateUser(username, uri)
    }

}