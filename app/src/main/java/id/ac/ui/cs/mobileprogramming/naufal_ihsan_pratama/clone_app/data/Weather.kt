package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data

import com.google.gson.annotations.SerializedName

data class Weather(
    @SerializedName("weather_state_abbr")
    val name: String?,
    @SerializedName("the_temp")
    val temp: String?
)

data class ConsolidateWeather(
    @SerializedName("consolidated_weather")
    val weathers: List<Weather>? = null
)