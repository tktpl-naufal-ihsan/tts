package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var viewModel: ListViewModel
    private val listAdapter = ListAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(ListViewModel::class.java)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        viewModel.refesh()

        itemsList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }

        observeViewModel()

    }

    fun observeViewModel() {
        viewModel.users.observe(this, Observer { tweet ->
            tweet?.let {
                if (it.isNotEmpty()) {
                    itemsList.visibility = View.VISIBLE
                    listAdapter.updateList(tweet)
                } else {
                    emptyState.visibility = View.VISIBLE
                }
            }
        })

        viewModel.error.observe(this, Observer { isError ->
            isError?.let {
                emptyState.visibility = if (it) View.VISIBLE else View.GONE
            }

        })

        viewModel.loading.observe(this, Observer { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    emptyState.visibility = View.GONE
                    itemsList.visibility = View.GONE
                }
            }
        })

    }
}