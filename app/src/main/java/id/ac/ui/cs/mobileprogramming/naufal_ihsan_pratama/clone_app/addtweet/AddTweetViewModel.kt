package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.addtweet

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Tweet
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.TweetsRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.UsersRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.CloneDatabase
import kotlinx.coroutines.launch

class AddTweetViewModel(application: Application) : AndroidViewModel(application) {

    private var PRIVATE_MODE = 0
    private val PREF_NAME = "clone-app"

    private val repository: TweetsRepository
    private val repository2: UsersRepository

    val event = MutableLiveData<Boolean>()
    val user = MutableLiveData<User>()

    private var username: String = ""
    private val sharedPrefs by lazy {
        getApplication<Application>().getSharedPreferences(
            PREF_NAME,
            PRIVATE_MODE
        )
    }

    init {
        val tweetDao = CloneDatabase.getDatabase(application).tweetDao()
        repository = TweetsRepository(tweetDao)

        val usersDao = CloneDatabase.getDatabase(application).userDao()
        repository2 = UsersRepository(usersDao)
        username = sharedPrefs.getString("username", "user")!!
    }

    fun fetch() {
        getUser()
    }

    fun getUser() = viewModelScope.launch {
        user.value = repository2.get(username)
    }


    fun addTweet(text: String) = viewModelScope.launch {
        repository.add(Tweet(0, username, text, "", System.currentTimeMillis() / 1000))
        event.value = true
    }
}
