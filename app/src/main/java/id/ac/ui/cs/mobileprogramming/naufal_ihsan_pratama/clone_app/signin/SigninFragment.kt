package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.signin

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.databinding.SigninFragmentBinding
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.home.Homepage
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import kotlinx.android.synthetic.main.signin_fragment.*


class SigninFragment : Fragment() {

    private lateinit var viewModel: SigninViewModel
    private lateinit var binding: SigninFragmentBinding

    private val username = "userInput"
    private val password = "passInput"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.signin_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this)
            .get(SigninViewModel::class.java)

        if (savedInstanceState != null) {
            signInUsername.setText(savedInstanceState.getString(username))
            signUpPass.setText(savedInstanceState.getString(password))
        }

        signInBtn.setOnClickListener {
            val username = signInUsername.text.toString()
            val password = signUpPass.text.toString()

            viewModel.signIn(username, password)

            viewModel.user.observe(this, Observer {
                if (it != null) {
                    val intent = Intent(
                        context,
                        Homepage::class.java
                    )
                    intent.putExtra("username", it.username)
                    intent.putExtra("imgurl", it.imgurl)
                    startActivity(intent)
                }
            })
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(username, signInUsername.text.toString())
        outState.putString(password, signUpPass.text.toString())
    }
}
