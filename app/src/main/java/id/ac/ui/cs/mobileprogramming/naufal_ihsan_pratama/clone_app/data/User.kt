package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User @JvmOverloads constructor(
    @PrimaryKey @ColumnInfo(name = "username") var username: String = "",
    @ColumnInfo(name = "email") var email: String = "",
    @ColumnInfo(name = "password") var password: String = "",
    @ColumnInfo(name = "imgurl") var imgurl: String = ""
) {
    val isEmpty
        get() = username.isEmpty() || email.isEmpty() || password.isEmpty()
}
