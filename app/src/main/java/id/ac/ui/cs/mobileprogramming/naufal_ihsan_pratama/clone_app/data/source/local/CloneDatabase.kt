package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Follow
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Tweet
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User

@Database(entities = [User::class, Tweet::class, Follow::class], version = 6, exportSchema = false)
abstract class CloneDatabase : RoomDatabase() {

    abstract fun userDao(): UsersDao
    abstract fun tweetDao(): TweetsDao
    abstract fun followDao(): FollowsDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: CloneDatabase? = null

        fun getDatabase(context: Context): CloneDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CloneDatabase::class.java,
                    "clone_app_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}