package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.addtweet

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R

class AddTweet : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_tweet_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, AddTweetFragment.newInstance())
                .commitNow()
        }
    }

}
