package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.Follow
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User

@Dao
interface FollowsDao {

    @Query("SELECT COUNT(follower) FROM Follow WHERE follower = :username  ")
    suspend fun getFollowing(username: String): Int

    @Query("SELECT COUNT(following) FROM Follow WHERE following = :username  ")
    suspend fun getFollower(username: String): Int

    @Query("SELECT * FROM User WHERE username NOT IN(SELECT following FROM Follow WHERE follower = :username) AND username != :username")
    suspend fun findUsers(username: String): List<User>

    @Insert
    suspend fun isFollow(follow: Follow)
}