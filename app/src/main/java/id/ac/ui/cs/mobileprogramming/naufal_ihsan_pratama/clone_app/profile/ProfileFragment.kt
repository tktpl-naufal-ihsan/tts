package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.profile

import android.Manifest
import android.app.Activity
import android.app.AlarmManager
import android.app.AlertDialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.work.*
import com.bumptech.glide.Glide
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.util.RealPathUtil
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.util.DownLoadFileWorkManager
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.util.NotificationsHelper
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


@Suppress("DEPRECATION")
class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel

    val workManager = WorkManager.getInstance()

    private var currentPhotoPath: String = ""
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_GALLERY = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel =
            ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        viewModel.fetch()


        viewModel.user.observe(this, Observer { user ->
            username.text = "@".plus(user.username)
            try {
                if (user.imgurl != "-") {
                    Glide.with(this).load(user.imgurl).into(tweetImg)
                } else {
                    Glide.with(this)
                        .load(R.drawable.placeholder)
                        .into(tweetImg)
                }

            } catch (e: Exception) {
                Glide.with(this).load(Uri.fromFile(File(user.imgurl))).into(tweetImg)
            }

        })

        viewModel.icon.observe(this, Observer {
            Glide.with(this).load(it).into(weatherIcon)
        })

        viewModel.temp.observe(this, Observer {
            weatherDegree.text = it.toString()
        })

        viewModel.following.observe(this, Observer {
            followingNum.text = it.toString()
        })

        viewModel.follower.observe(this, Observer {
            followerNum.text = it.toString()
        })

        tweetImg.setOnClickListener {
            val permission = ContextCompat.checkSelfPermission(
                activity?.baseContext!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            if (permission != PackageManager.PERMISSION_GRANTED) {
                showWarningDialog("Please allow access to write on external storage")
            } else {
                showPictureDialog()
            }
        }


        privacyPolicy.setOnClickListener {
            val connMgr =
                activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val network = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

            if (network.isConnectedOrConnecting) {
                StartOneTimeWorkManager()
            } else {
                showWarningDialog("Connect to WiFi caused Download Large File")
            }
        }
    }


    private fun showWarningDialog(text: String) {
        val warningDialog = AlertDialog.Builder(context)
        warningDialog.setTitle("Caution")
        warningDialog.setMessage(text)
        warningDialog.setNeutralButton("Close") { _, _ -> }
        warningDialog.show()
    }

    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(context)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { _, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }


    private fun takePhotoFromCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            context?.packageManager?.let {
                takePictureIntent.resolveActivity(it)?.also {
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        // Error occurred while creating the File
                        null
                    }

                    photoFile?.also { it ->
                        val photoURI: Uri = FileProvider.getUriForFile(
                            context!!,
                            "id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                    }
                }
            }
        }
    }

    private fun choosePhotoFromGallery() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var path = ""
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                path = currentPhotoPath
            } else if (requestCode == REQUEST_IMAGE_GALLERY) {
                val contentURI = data!!.data
                path = RealPathUtil.getRealPath(context!!, contentURI!!).toString()
            }
            viewModel.updateImg(path)
            viewModel.fetch()
        }
    }

    private fun StartOneTimeWorkManager() {
        val constraints =
            Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
        val task = OneTimeWorkRequest.Builder(DownLoadFileWorkManager::class.java)
            .setConstraints(constraints).build()

        workManager.enqueue(task)

        workManager.getWorkInfoByIdLiveData(task.id)
            .observe(this, Observer {
                it?.let {

                    if (it.state == WorkInfo.State.RUNNING) {
                        Toast.makeText(context, "Download Start", Toast.LENGTH_SHORT).show()
                    }

                    if (it.state.isFinished) {
                        Toast.makeText(context, "Download Complete", Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

}