package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local

import androidx.room.*
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User

@Dao
interface UsersDao {

    @Query("SELECT * FROM User WHERE username = :username AND password = :password")
    suspend fun authenticate(username: String, password: String): User?

    @Query("SELECT * FROM User WHERE username = :username")
    suspend fun getUser(username: String): User?

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun addUser(user: User)

    @Query("UPDATE User SET imgurl = :imgurl WHERE username = :username")
    suspend fun updateUser(username: String, imgurl: String)

}