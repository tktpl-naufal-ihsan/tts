package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.signup

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.*
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.UsersRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.CloneDatabase
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*

class SignupViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: UsersRepository
    val user = MutableLiveData<User>()


    init {
        val userDao = CloneDatabase.getDatabase(application).userDao()
        repository = UsersRepository(userDao)
    }

    fun signUp(username: String, email: String, password: String) = viewModelScope.launch {
        try {
            repository.insert(User(username, email, password, "-"))
            val login = repository.auth(username, password)
            user.value = login
            if (user.value != null) {
                Toast.makeText(
                    getApplication(),
                    "Log in as " + user.value!!.username,
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else {
                Toast.makeText(getApplication(), "Login Failed", Toast.LENGTH_SHORT)
                    .show()
            }
        } catch (e: Exception) {
            Toast.makeText(
                getApplication(),
                "Username ${username} is not available",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
