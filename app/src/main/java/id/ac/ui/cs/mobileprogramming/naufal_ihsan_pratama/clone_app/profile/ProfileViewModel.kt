package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.profile

import android.app.Application
import androidx.lifecycle.*
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.R
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.ConsolidateWeather
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.User
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.FollowsRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.UsersRepository
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.WeatherService
import id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.data.source.local.CloneDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    private var PRIVATE_MODE = 0
    private val PREF_NAME = "clone-app"

    val user = MutableLiveData<User>()
    val event = MutableLiveData<Boolean>(false)
    val icon = MutableLiveData<Int>()
    val temp = MutableLiveData<String>()
    val following = MutableLiveData<Int>()
    val follower = MutableLiveData<Int>()

    private val weatherService = WeatherService()
    private val disposable = CompositeDisposable()

    private val repository: UsersRepository
    private val repository2: FollowsRepository

    private var username: String = ""
    private val sharedPrefs by lazy {
        getApplication<Application>().getSharedPreferences(
            PREF_NAME,
            PRIVATE_MODE
        )
    }

    init {
        val usersDao = CloneDatabase.getDatabase(application).userDao()
        val followsDao = CloneDatabase.getDatabase(application).followDao()
        repository = UsersRepository(usersDao)
        repository2 = FollowsRepository(followsDao)
        username = sharedPrefs.getString("username", "user")!!
    }

    fun fetch() {
        getUser()
        fetchFromRemote()
    }


    private fun getUser() = viewModelScope.launch {
        user.value = repository.get(username)
        follower.value = repository2.getFollower(username)
        following.value = repository2.getFollowing(username)
    }


    fun updateImg(uri: String) = viewModelScope.launch {
        repository.update(username, uri)
    }


    private fun fetchFromRemote() {
        disposable.add(
            weatherService.getWeather()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ConsolidateWeather>() {
                    override fun onSuccess(sources: ConsolidateWeather) {
                        if (sources.weathers?.get(0)?.name.toString().contains("c")) {
                            icon.value = R.drawable.ic_cloudy
                        } else {
                            icon.value = R.drawable.ic_rainy
                        }

                        temp.value = sources.weathers?.get(0)?.temp.toString().plus(" °C")
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }

                })
        )
    }

}