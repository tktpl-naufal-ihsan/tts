package id.ac.ui.cs.mobileprogramming.naufal_ihsan_pratama.clone_app.util

import android.content.Context
import android.os.Environment

import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class DownLoadFileWorkManager(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): ListenableWorker.Result {
        try {

            NotificationsHelper(applicationContext).createDownloadNofitication(
                "Download File",
                "Download Privacy and Policy PDF",
                0
            )

            // example google pdf file
            val url =
                URL("https://file-examples.com/wp-content/uploads/2017/10/file-example_PDF_500_kB.pdf")
            val connection = url.openConnection()
            connection.connect()

            // input stream to read file
            val fileLength = connection.contentLength
            val input = BufferedInputStream(url.openStream(), 8192)


            val storageDirs: File? =
                applicationContext.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val file: File? = File.createTempFile("PDF_${timeStamp}_", ".pdf", storageDirs)

            // Output stream to write file
            val output = FileOutputStream(file)

            val data = ByteArray(1024)

            var count: Int = 0

            var total: Int = 0
            var percentage: Int = 0

            while ({ count = input.read(data);count }() != -1) {
                total += count
                percentage = ((total * 100) / fileLength)

                NotificationsHelper(applicationContext).createDownloadNofitication(
                    "Download File",
                    "Download Privacy and Policy PDF",
                    percentage
                )
                output.write(data, 0, count)
            }

            // flushing output
            output.flush()

            // closing streams
            output.close()
            input.close()

        } catch (e: Exception) {
            return Result.retry()
        }

        NotificationsHelper(applicationContext).createDownloadNofitication(
            "Download File",
            "Download Privacy and Policy PDF",
            100
        )

        return Result.success()
    }
}